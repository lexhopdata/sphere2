import React, {
  Fragment,
  useState,
  useEffect
} from 'react';

import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import raw from 'raw.macro';

const SLUG='dopomoga-gromadyanam';

const HelpCitizens = () => {
  return (
    <div className="text text-with-images">
      <ReactMarkdown>{ raw(`./data/${SLUG}.md`) }</ReactMarkdown>
    </div>
  )
}

export default HelpCitizens;
