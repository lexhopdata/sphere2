import React from 'react';

const Header = () => {
  return (
    <div className="main-header">
      <img src="/header.jpg" className="main-header-image" />
    </div>
  );
}

export default Header;
