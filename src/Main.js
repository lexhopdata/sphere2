import React from 'react';
import { Link } from 'react-router-dom'

import ReactMarkdown from 'react-markdown'
import raw from 'raw.macro';
import PayBanner from './PayBanner';
import HowToHelp from './HowToHelp';

const MAIN_SLUG='golovna'

const Main = () => {
  return (
    <div className="text text-with-images">
      <PayBanner />

      <ReactMarkdown children={raw(`./data/${MAIN_SLUG}.md`)} />

      <h2 className="head">Наші проекти</h2>
      <div className="flex-row our-projects">

        <div className="flex-column box">
          <Link to="/dopomoga-voinam">
            <img src="/dopomoga-voinam.jpg" />
            <div className="label">Допомога воїнам</div>
          </Link>
        </div>

        <div className="flex-column box">
          <Link to="/dopomoga-gromadyanam">
            <img src="/dopomoga-gromadyanam.jpg" />
            <div className="label">Допомога громадянам</div>
          </Link>
        </div>

        <div className="flex-column box">
          <Link to="/dopomoga-tvarynam">
            <img src="/dopomoga-tvarynam.jpg" />
            <div className="label">Порятунок осиротілих тварин</div>
          </Link>
        </div>

      </div>

      <h2 className="head">Як допомогти</h2>

      <HowToHelp />

    </div>
  )
}

export default Main;
