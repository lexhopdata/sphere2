import React from 'react';
import { Link } from 'react-router-dom'

const PayBanner = () => {
  return (
    <Link to="/rahunky">
      <div className="pay-banner">
        <div className="pay-banner-text">Онлайн UAH / USD / EUR</div>
        <div className="pay-banner-button">Допомогти</div>
      </div>
    </Link>
  )
}

export default PayBanner;
