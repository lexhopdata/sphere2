import React from 'react';
import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import raw from 'raw.macro';

const Footer = () => (
  <footer className="footer text order10 main-grid-footer">
    <ReactMarkdown
      children={raw(`./data/podval.md`)}
      remarkPlugins={[remarkGfm]}
      components={{
        a: AnchorCustom,
      }}
    />
  </footer>
)

const AnchorCustom = ({node, ...props}) => (
  /* eslint-disable-next-line react/jsx-props-no-spreading */
  <a {...props} target="_blank" rel="noreferrer" />
)

export default Footer;
