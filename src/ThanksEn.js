import React from 'react';

import ReactMarkdown from 'react-markdown'
import raw from 'raw.macro';

const SLUG='podyaka-en';

const Thanks = () => {
  return (
    <div className="text text-with-images">
      <h2>Thank You</h2>
      <p className="epigraph">
        <i>Only a few believe in the good who create it.</i>
        <br />
        Maria von Ebner-Eschenbach
      </p>
      <ReactMarkdown>{ raw(`./data/${SLUG}.md`) }</ReactMarkdown>
    </div>
  )
}

export default Thanks;
