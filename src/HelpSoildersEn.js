import React from 'react';

import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import raw from 'raw.macro';
import HowToHelpEn from './HowToHelpEn';

const SLUG='dopomoga-voinam-en';

const HelpSoildersEn = () => {
  return (
    <div className="text text-with-images">
      <h2 className="head">Help the soldiers</h2>

      <div className="help-soilders text">
        <ReactMarkdown
          children={raw(`./data/${SLUG}.md`)}
          remarkPlugins={[remarkGfm]}
        />
      </div>

      <HowToHelpEn />

    </div>
  )
}

export default HelpSoildersEn;
