import React from 'react';
import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import raw from 'raw.macro';

const FooterEn = () => (
  <div className="footer text">
    <ReactMarkdown
      children={raw(`./data/podval-en.md`)}
      remarkPlugins={[remarkGfm]}
      components={{
        a: AnchorCustom,
      }}
    />
  </div>
)

const AnchorCustom = ({node, ...props}) => (
  /* eslint-disable-next-line react/jsx-props-no-spreading */
  <a {...props} target="_blank" rel="noreferrer" />
)

export default FooterEn;
