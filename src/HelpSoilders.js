import React, {
  Fragment,
  useState,
  useEffect
} from 'react';

import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import raw from 'raw.macro';
import HowToHelp from './HowToHelp';

const SLUG='dopomoga-voinam';

const HelpSoilders = () => {
  return (
    <div className="text text-with-images">
      <h2 className="head">Допомога воїнам</h2>

      <div className="help-soilders text">
        <ReactMarkdown
          children={raw(`./data/${SLUG}.md`)}
          remarkPlugins={[remarkGfm]}
        />
      </div>

      <HowToHelp />

    </div>
  )
}

export default HelpSoilders;
