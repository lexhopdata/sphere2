import React from 'react';
import { Link } from 'react-router-dom';
import Logo from './Logo';

const Menu = () => (
  <div className="main-grid-menu">
    <Logo />
    <ul className="empty-list main-menu">
      <li>
        <Link to="/">Головна</Link>
      </li>
      <li>
        <Link to="/dopomoga-voinam">Допомога воїнам</Link>
      </li>
      <li>
        <Link to="/dopomoga-gromadyanam">Допомога громадянам</Link>
      </li>
      <li>
        <Link to="/dopomoga-tvarynam">Допомога осиротілим тваринам</Link>
      </li>
      <li>
        <Link to="/podyaka">Наші благодійники</Link>
      </li>
      <li>
        <Link to="/fotozvity">Фотозвіти</Link>
      </li>
      <li>
        <Link to="/pro-nas">Про нас</Link>
      </li>
    </ul>
  </div>
)

export default Menu;
