import React from 'react';
import { Link } from 'react-router-dom';
import LogoEn from './LogoEn';

const Menu = () => (
  <div className="main-grid-menu">
    <LogoEn />
    <ul className="empty-list main-menu">
      <li>
        <Link to="/en">Home</Link>
      </li>
      <li>
        <Link to="/en/help-soilders">Help Army</Link>
      </li>
      <li>
        <Link to="/en/thanks">Thank You</Link>
      </li>
      <li>
        <Link to="/en/accounts">Accounts</Link>
      </li>
      <li>
        <Link to="/en/about">About Us</Link>
      </li>
    </ul>
  </div>
)

export default Menu;
