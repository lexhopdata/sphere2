import React from 'react';
import ReactMarkdown from 'react-markdown'
import raw from 'raw.macro';

const SLUG='dopomoga-tvarynam';

const HelpAnimals = () => {
  return (
    <div className="text text-with-images">
      <ReactMarkdown>{ raw(`./data/${SLUG}.md`) }</ReactMarkdown>
    </div>
  )
}

export default HelpAnimals;
