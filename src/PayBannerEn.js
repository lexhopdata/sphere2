import React from 'react';
import { Link } from 'react-router-dom'

const PayBannerEn = () => {
  return (
    <Link to="/en/accounts">
      <div className="pay-banner">
        <div className="pay-banner-text">Online UAH / USD / EUR</div>
        <div className="pay-banner-button">Help</div>
      </div>
    </Link>
  )
}

export default PayBannerEn;
