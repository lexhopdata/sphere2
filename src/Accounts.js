import React from 'react';

import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import raw from 'raw.macro';

const SLUG='rahunky';

const Accounts = () => {
  return (
    <div className="text text-with-images order20 accounts">

      <ReactMarkdown
        remarkPlugins={[remarkGfm]}
        components={{
          table: TableCustom,
        }}
      >
        { raw(`./data/${SLUG}.md`) }
      </ReactMarkdown>
    </div>
  )
}

const TableCustom = ({node, ...props}) => (
  /* eslint-disable-next-line react/jsx-props-no-spreading */
  <table {...props} className="md-table" />
)

export default Accounts;
