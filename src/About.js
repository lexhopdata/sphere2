import React, {
  Fragment,
  useState,
  useEffect
} from 'react';

import ReactMarkdown from 'react-markdown'
import raw from 'raw.macro';

const SLUG='pro-nas';
const md = raw(`./data/${SLUG}.md`)

const About = () => {
  return (
    <div className="text text-with-images">
      <ReactMarkdown>{ raw(`./data/${SLUG}.md`) }</ReactMarkdown>
    </div>
  )
}


export default About;
