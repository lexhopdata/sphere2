import React from 'react';
import { Link } from 'react-router-dom'

const HowToHelpEn = () => {
  return (
    <div className="flex-row how-to-help">
      <div className="box">
        <h3>Send</h3>
        <p>
          If you have any of the necessary lists, you can send them to us for further transfer to the destination.
        </p>
        <p>
          Branch addresses:
          <br />
          Nova Poshta - 145 Kyiv;
          <br />
          UkrPoshta - 04214 Kyiv.
          <br />
          Andrii Chernyakov
          <br />
          Tel:
          <a href="tel:+380509980068">+380 50 9980068</a>
        </p>
      </div>
      <div className="box">
        <h3>Buy</h3>
        <p>
          You can buy anything from the current list of needs, and send it to us for further transfer to the destination.
        </p>
        <p>
          Branch addresses:
          <br />
          Nova Poshta - 145 Kyiv;
          <br />
          UkrPoshta - 04214 Kyiv.
          <br />
          Andrii Chernyakov
          <br />
          Tel:
          <a href="tel:+380509980068" target="_blank" rel="noreferrer">
            +380 50 9980068
          </a>
        </p>
      </div>
      <div className="box">
        <h3>Transfer</h3>
        <Link to="/en/accounts">
          <p>
            Transfer money online with a bank card or account
          </p>
        </Link>
      </div>
    </div>
  )
}

export default HowToHelpEn;
