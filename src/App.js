import React from 'react';

import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

import HelpAnimals from './HelpAnimals';
import HelpCitizens from './HelpCitizens';
import HelpSoilders from './HelpSoilders';
import Thanks from './Thanks';
import Reports from './Reports';
import About from './About';
import Main from './Main';
import Accounts from './Accounts';
import Menu from './Menu';
import Footer from './Footer';

import MainEn from './MainEn';
import HelpSoildersEn from './HelpSoildersEn';
import ThanksEn from './ThanksEn';
import AccountsEn from './AccountsEn';
import AboutEn from './AboutEn';
import MenuEn from './MenuEn';
import FooterEn from './FooterEn';
import Header from './Header';


function App() {
  return (
    <Router>
      <Routes>
        {/* Note: sync createEntryPoints.sh */}
        <Route exact path="/" element={<Page content={<Main />} />} />
        <Route path="/dopomoga-voinam" element={<Page content={<HelpSoilders />} />} />
        <Route path="/dopomoga-gromadyanam" element={<Page content={<HelpCitizens />} />} />
        <Route path="/dopomoga-tvarynam" element={<Page content={<HelpAnimals />} />} />
        <Route path="/podyaka" element={<Page content={<Thanks />} />} />
        <Route path="/fotozvity" element={<Page content={<Reports />} />} />
        <Route path="/pro-nas" element={<Page content={<About />} />} />
        <Route path="/rahunky" element={<Page content={<Accounts />} />} />

        <Route path="/en" element={<PageEn content={<MainEn />} />} />
        <Route path="/en/help-soilders" element={<PageEn content={<HelpSoildersEn />} />} />
        <Route path="/en/thanks" element={<PageEn content={<ThanksEn />} />} />
        <Route path="/en/accounts" element={<PageEn content={<AccountsEn />} />} />
        <Route path="/en/about" element={<PageEn content={<AboutEn />} />} />
      </Routes>
    </Router>
  );
}

function Page({ content }) {
  return (
    <div className="main-grid">
      <Menu />
      <div className="main-grid-content">
        <Header />
        { content }
      </div>
      <Footer />
    </div>
  );
}

function PageEn({ content }) {
  return (
    <div className="main-grid">
      <MenuEn />
      <div className="main-grid-content">
        <Header />
        { content }
        <FooterEn />
      </div>
    </div>
  );
}


export default App;
