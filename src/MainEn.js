import React from 'react';
import { Link } from 'react-router-dom'

import ReactMarkdown from 'react-markdown'
import raw from 'raw.macro';
import PayBannerEn from './PayBannerEn';
import HowToHelpEn from './HowToHelpEn';

const MAIN_SLUG='golovna-en'

const Main = () => {
  return (
    <div className="text text-with-images">
      <PayBannerEn />

        <ReactMarkdown children={raw(`./data/${MAIN_SLUG}.md`)} />

        <h2 className="head">Our Projects</h2>
        <div className="flex-row our-projects">

          <div className="flex-column box">
            <Link to="/en/help-soilders">
              <img src="dopomoga-voinam.jpg" />
              <div className="label">Help Warriors</div>
            </Link>
          </div>

          <div className="flex-column box">
            {/* <Link to="/en/help-citizens"> */}
              <img src="/dopomoga-gromadyanam.jpg" />
              <div className="label">Help Citizens</div>
              {/* </Link> */}
          </div>

          <div className="flex-column box">
            {/* <Link to="/en/help-animals"> */}
              <img src="/dopomoga-tvarynam.jpg" />
              <div className="label">Salvation Orphaned Animals</div>
            {/* </Link> */}
          </div>

        </div>

        <h2 className="head">How To Help</h2>

        <HowToHelpEn />

    </div>
  )
}

export default Main;
