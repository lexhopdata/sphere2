import React from 'react';
import { Link } from 'react-router-dom';

const LogoEn = () => (
  <div className="logo">
    <div className="lang-panel flex-row">
      <Link to="/"><img src="/uk.png" className="lang-image" /></Link>
      <Link to="/en"><img src="/en.png" className="lang-image" /></Link>
    </div>
    <Link to="/en">
      <img src="/logo.jpg" className="logo-image" alt="" />
    </Link>
  </div>
)

export default LogoEn;
