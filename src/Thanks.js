import React, {
  Fragment,
  useState,
  useEffect
} from 'react';

import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import raw from 'raw.macro';

const SLUG='podyaka';

const md = raw(`./data/${SLUG}.md`)

const Thanks = () => {
  return (
    <div className="text text-with-images">
      <h2>Наші благодійники</h2>
      <p className="epigraph">
        <i>В добро вірять лише ті  деякі, хто його творить.</i>
        <br />
        Марія фон Ебнер-Ешенбах
      </p>
      <ReactMarkdown>{ md }</ReactMarkdown>
    </div>
  )
}

export default Thanks;
