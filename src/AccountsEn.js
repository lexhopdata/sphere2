import React, {
  Fragment,
  useState,
  useEffect
} from 'react';

import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import raw from 'raw.macro';
import Footer from './Footer';

const SLUG='rahunky';  // same file: "rahunky"

const AccountsEn = () => {
  return (
    <div className="text text-with-images order20 accounts">

      <ReactMarkdown
        remarkPlugins={[remarkGfm]}
        components={{
          table: TableCustom,
        }}
      >
        { raw(`./data/${SLUG}.md`) }
      </ReactMarkdown>
    </div>
  )
}

const TableCustom = ({node, ...props}) => (
  /* eslint-disable-next-line react/jsx-props-no-spreading */
  <table {...props} className="md-table" />
)

export default AccountsEn;
