import React from 'react';

import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import raw from 'raw.macro';

const SLUG='fotozvity';

const Reports = () => {
  return (
    <div className="text text-with-images order20 accounts">
      <ReactMarkdown
        remarkPlugins={[remarkGfm]}
      >
        { raw(`./data/${SLUG}.md`) }
      </ReactMarkdown>
    </div>
  )
}

export default Reports;
