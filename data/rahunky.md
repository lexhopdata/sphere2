##### UAH

| Номер рахунку UAH: | UA10 3808 0500 0000 0026 0007 7643 9       |
| ------------------ | ------------------------------------------ |
| Назва організації: | ВСЕУКРАЇНСЬКА БЛАГОДІЙНА ОРГАНІЗАЦІЯ «Сфера-ЗЕТ»  |
| Юридична адреса:   | 79024, вул.Молочна 15А, м.Львів. Україна         |

##### USD

**Бенефіціар (Отримувач) BENEFICIARY**
| Номер рахунку USD      | UA14 3808 0500 0000 0026 0087 7644 2             |
|-----------------------|---------------------------------------------------|
| Назва організації     |   ALL-UKRAINIAN CHARITABLE ORGANIZATION SFERA-ZET  |
| Юридична адреса       |  79024, 15A Molochna Street, Lviv, Ukraine       |

**Банк Бенефіціара (Отримувача) BANK OF BENEFICIARY**
|                                                                           |                        |                                     |
|---------------------------------------------------------------------------|------------------------|-------------------------------------|
|Raiffeisen Bank Joint Stock Company, Leskova street.9, KYIV 01011, Ukraine | SWIFTcode: AVALUAUKXXX | Correspondent Account: 890-0260-688 |

**Банк Посередник (кореспондент) CORRESPONDENT BANK**
|                                           |                     |
|-------------------------------------------|---------------------|
|The Bank of New York Mellon, New York, USA | SWIFTcode: IRVTUS3N |


##### EUR

|Номер рахунку EUR| UA88 3808 0500 0000 0026 0097 7644 1             |
|-----------------|--------------------------------------------------|
|Назва організації| ALL-UKRAINIAN CHARITABLE ORGANIZATION SFERA-ZET   |
|Юридична адреса  | 79024, 15A Molochna Street, Lviv, Ukraine        |

**Банк Бенефіціара (Отримувача) BANK OF BENEFICIARY**
|                                                                           |                        |                                  |
|---------------------------------------------------------------------------|------------------------|----------------------------------|
|Raiffeisen Bank Joint Stock Company, Leskova street.9, KYIV 01011, Ukraine | SWIFTcode: AVALUAUKXXX | Correspondent Account 55.022.305 |


**Банк Посередник (кореспондент) CORRESPONDENT BANK**
|                                  |                        |
|----------------------------------|------------------------|
| Raiffeisen Bank International AG |  SWIFTcode: RZBAAT WW  |

