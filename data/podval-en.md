**ALL-UKRAINIAN CHARITABLE ORGANIZATION "SPHERE OF HARMONY, EVOLUTION, CONCERN"**\
Identification code: 38370530; Address: 79024, 15A Molochna Street, Lviv, Ukraine; [a.rezekulov@prozet.com.ua](mailto:a.rezekulov@prozet.com.ua)

**Viber/WhatsApp/Telegram:**
[+38 098 6071975](tel:+380986071975),
[+38 050 9980068](tel:+380509980068)
