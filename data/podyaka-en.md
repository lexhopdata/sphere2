Great honor and low bow to each of you who did not stay away from the problems of Ukraine!

We understand that it is not possible to provide everything that is so necessary at the front. But for everything you have managed to do thanks to us, we are grateful both on our own behalf and on behalf of the boys and girls who are desperately defending our Motherland and closing the rear!

