ALL-UKRAINIAN CHARITABLE ORGANIZATION "SPHERE OF HARMONY, EVOLUTION, CONCERN" was established in 2012.

The goal and objectives of the UBO have been, are and will be comprehensive and multifaceted social assistance to those who really need support, from free food to free housing. Including: assistance to gifted children from low-income families in obtaining specialized education; assistance to volunteer organizations that solve the problems of stray animals and much more.

The war that came to our house made its adjustments.

Today we have focused our activities exclusively on three current projects and we are counting on YOUR help!

For us, each of your parcels, each of your one hryvnia, dollar, euro is priceless!

