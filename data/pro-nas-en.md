## ABOUT US

ALL-UKRAINIAN CHARITABLE ORGANIZATION "SPHERE OF HARMONY, EVOLUTION, CONCERN"

It was established in 2012 at the initiative of the team of developers of the socio-economic project "Healthy Economy of Trade Program".

The goal and objectives of the UBO were and are comprehensive and multifaceted social assistance to those who really need support, from free food to free housing. Including: assistance to gifted children from low-income families in obtaining specialized education; assistance to volunteer organizations that solve the problems of stray animals and much more. Funding for all charitable projects is included in the budget of "Healthy Economy of Trade Program", the full-scale launch of which was scheduled for 2022.

The war that came to our house made its adjustments.

Today we have focused our activities exclusively on three current projects and we are counting on YOUR help!

